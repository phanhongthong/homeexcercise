const config = require("config");
const mongoose = require("mongoose");
const dbURI = config.get("mongoURI");

const connectDB = async () => {
    try {
        await mongoose.connect(dbURI, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true
        });

        console.log("db connected");
    } catch (err) {
        console.log(err);
        process.exit();
    }
};

module.exports = connectDB;
